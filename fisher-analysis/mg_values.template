[runtime]
sampler = test

[test]
save_dir=cosmosis-ska-forecasting/fisher_results/

[multinest]
max_iterations=50000
live_points=500
multinest_outfile_root=XXX

;[runtime]
; The test sampler just runs a single parameter set
;sampler = emcee

[emcee]
walkers = 64
samples = 5000
nsteps = 10


[output]
format=text
filename=XXX.txt
verbosity=noisy

; The pipeline section contains information
; that describes the sequence of calculations
; to be done and what we want out at the end
[pipeline]
; The list of modules to be run, in this order.
; The modules named here must appear as sections below
modules = consistency isitgr halofit ska_number_density shear_shear_mg cl_like
values = cosmosis-ska-forecasting/analysis/mg_values.ini

likelihoods = WL
extra_output = cosmological_parameters/sigma_8

; We can get a little more output during the run by setting some values
quiet=F
timing=F
debug=T

[consistency]
file = cosmosis-standard-library/utility/consistency/consistency_interface.py

[cl_like]
file=cosmosis-ska-forecasting/likelihood/cl_like.py
dirname=XXX

[isitgr]
file = cosmosis-standard-library/boltzmann/isitgr/camb.so
mode=all
lmax=2500
feedback=0
zmax=10.0
nz=491
use_r_function = F
scale_dependent = F


[halofit]
file = cosmosis-standard-library/boltzmann/halofit/halofit_module.so

; This module uses the Limber approximation to compute shear-shear C_ell
; given the shear kernel (which is derived from the number density and 
; from geometry)
[shear_shear_mg]
file = cosmosis-standard-library/shear/spectra-ppf/interface.so
ell_min = 20.0
ell_max = 10000.0
n_ell = 150
intrinsic_alignments=F

[simulate_dataset]
ellmin = 20.
ellmax = 3000.
nell = 50
sigma_e = 0.3

; Parameters and data in CosmoSIS are organized into sections
; so we can easily see what they mean.
; There is only one section in this case, called cosmological_parameters
[cosmological_parameters]

; These parameters are pretty much always required
; for cosmology, though you sometimes just fix omega_b
omega_m = 0.2 0.3 0.5
h0 = 0.4 0.72  1.1
omega_b = 0.02 0.04 0.06

; Tau (optical depth) is only needed if you need
; the thermal history, or the CMB/matter power
tau = 0.08

; These ones are only needed if you are doing CMB or matter
; power data, not if you just want thermal history or background
; evolution
n_s = 0.7  0.96  1.22
A_s = 1.0e-9  2.1e-9  3.3e-9

; These parameters can affect everything but are not required - 
; if they are not found in this file they take the sensible
; default values shown here
omega_k = 0.0
w = -1.0
wa = 0.0

[post_friedmann_parameters]
d_0 = {value2}
d_inf = 1.0
q_0 = {value1}
q_inf = 1.0
s = 1.0
k_c = 0.01


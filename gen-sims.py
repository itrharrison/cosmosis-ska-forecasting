import os


#experiments = ['early', 'one', 'two']
experiments = ['cfhtlens', 'des', 'euclid', 'ska1-med', 'ska2-large', 'eucliduska2']

nz_experiments = {
    'cfhtlens': 'cfhtlens',
    'des' : 'des',
    'euclid' : 'euclid',
    'ska1-med':'ska1_med',
    'ska2-large':'ska2_large',
    'eucliduska2':'eucliduska2'
}


for expt in experiments:
    ini = 'cosmosis-ska-forecasting/configuration/{0}.ini'.format(expt)
    name = '{0}'.format(expt)
    nz_expt = nz_experiments[expt]
    dirname = 'cosmosis-ska-forecasting/datasets/{0}'.format(name)
    n_of_z = 'cosmosis-ska-forecasting/distributions/nofz_{0}.dat'.format(nz_expt)
    cmd = 'cosmosis {0} -p runtime.sampler=test pipeline.modules="consistency camb halofit ska_number_density shear_shear simulate_dataset"'.format(ini)
    print cmd
    
    w_ini = 'cosmosis-ska-forecasting/w-configuration/{0}.ini'.format(expt)
    name = '{0}'.format(expt)
    nz_expt = nz_experiments[expt]
    dirname = 'cosmosis-ska-forecasting/datasets/{0}'.format(name)
    n_of_z = 'cosmosis-ska-forecasting/distributions/nofz_{0}.dat'.format(nz_expt)
    w_cmd = 'cosmosis {0} -p runtime.sampler=test pipeline.modules="consistency camb halofit ska_number_density shear_shear simulate_dataset"'.format(w_ini)
    print w_cmd
    
    w0wa_ini = 'cosmosis-ska-forecasting/w0wa-configuration/{0}.ini'.format(expt)
    name = '{0}'.format(expt)
    nz_expt = nz_experiments[expt]
    dirname = 'cosmosis-ska-forecasting/datasets/{0}'.format(name)
    n_of_z = 'cosmosis-ska-forecasting/distributions/nofz_{0}.dat'.format(nz_expt)
    w0wa_cmd = 'cosmosis {0} -p runtime.sampler=test pipeline.modules="consistency camb halofit ska_number_density shear_shear simulate_dataset"'.format(w0wa_ini)
    print w0wa_cmd

    mg_ini = 'cosmosis-ska-forecasting/mg-configuration/{0}.ini'.format(expt)
    name = '{0}'.format(expt)
    nz_expt = nz_experiments[expt]
    dirname = 'cosmosis-ska-forecasting/datasets/{0}'.format(name)
    n_of_z = 'cosmosis-ska-forecasting/distributions/nofz_{0}.dat'.format(nz_expt)
    mg_cmd = 'cosmosis {0} -p runtime.sampler=test pipeline.modules="consistency isitgr halofit ska_number_density shear_shear_mg simulate_dataset"'.format(mg_ini)
    print mg_cmd

import os
import numpy as np
import pdb

start_dir = 'cosmosis-ska-forecasting/start/'

experiments = ['euclid', 'ska2-large', 'euclidxska2']

nz_experiments = {
    'euclid' : 'euclid',
    'ska2-large':'ska2_large',
    'euclidxska2':'eucliduxska2'
}

spaces = ['dark energy',
          #'matter',
          'mod grav']

parameters = {'dark energy' : np.array(['w0', 'wa']),
              'matter' : np.array(['sigma_8', 'omega_m']),
              'mod grav' : np.array(['q_0', 'd_0'])
              }
values = {'w0' : [-1.0, -1.05, -0.95, -1.025, -0.975],
          'wa' : [0.0, 0.05, -0.05, 0.025, -0.025],
          'sigma_8' : [],
          'omega_m' : [],
          'q_0' : [1.0, 0.95, 1.05, 1.025, 0.975],
          'd_0' : [1.0, 0.95, 1.05, 1.025, 0.975]
          }

template = open("cosmosis-ska-forecasting/launch-fisher.template").read()
matter_ini_template = open("cosmosis-ska-forecasting/analysis/matter_ini.template").read()
w0wa_ini_template = open("cosmosis-ska-forecasting/analysis/w0wa_ini.template").read()
mg_ini_template = open("cosmosis-ska-forecasting/analysis/mg_ini.template").read()


for expt in experiments:
  for space in spaces:
    pars = parameters[space]
    for fid in pars:
      val1 = values[fid][0]
      nfid = pars[~(pars==fid)][0]
      #pdb.set_trace()
      var = values[nfid]
      for val2 in var:
        #pdb.set_trace()
        name = 'fisher-{0}-{1}{2}-{3}{4}'.format(expt,
                                           fid, val1,
                                           nfid, val2)
        nz_expt = nz_experiments[expt]
        if (space=='matter'):
          ini = 'cosmosis-ska-forecasting/configuration/{0}.ini'.format(name)
          if fid=='sigma_8':
            ini_file = matter_ini_template.format(sigma_8=val1, omega_m=val2)
          else:
            ini_file = matter_ini_template.format(sigma_8=val2, omega_m=val1)
          open(ini, 'w').write(ini_file)
          out = 'cosmosis-ska-forecasting/results/{0}.txt'.format(name)
          data_dir = 'cosmosis-ska-forecasting/datasets/{0}'.format(name)
          cmd = 'cosmosis --mpi {0} '.format(ini)
          sub = template.format(cmd=cmd,name=name)
          sub_filename = 'cosmosis-ska-forecasting/launch/{0}.sub'.format(name)
          open(sub_filename,'w').write(sub)
          print "qsub ", sub_filename

        if (space=='dark energy'):
          w0wa_ini = 'cosmosis-ska-forecasting/w0wa-configuration/{0}.ini'.format(name)
          if fid=='w0':
            ini_file = w0wa_ini_template.format(w0=val1, wa=val2)
          else:
            ini_file = w0wa_ini_template.format(w0=val2, wa=val1)
          open(w0wa_ini, 'w').write(ini_file)
          w0wa_cmd = 'cosmosis --mpi {0} '.format(w0wa_ini)
          w0wa_sub = template.format(cmd=w0wa_cmd,name='w0wa_'+name)
          w0wa_sub_filename =  'cosmosis-ska-forecasting/launch/w0wa_{0}.sub'.format(name)
          w0wa_sub = template.format(cmd=w0wa_cmd,name=name)
          open(w0wa_sub_filename,'w').write(w0wa_sub)
          print "qsub ", w0wa_sub_filename

        if (space=='mod grav'):
          mg_ini = 'cosmosis-ska-forecasting/mg-configuration/{0}.ini'.format(name)
          if fid=='q_0':
            ini_file = mg_ini_template.format(q_0=val1, d_0=val2)
          else:
            ini_file = mg_ini_template.format(q_0=val2, d_0=val1)
          open(mg_ini, 'w').write(ini_file)
          mg_cmd = 'cosmosis --mpi {0} '.format(mg_ini)
          mg_sub = template.format(cmd=mg_cmd,name='mg_'+name)
          mg_sub_filename =  'cosmosis-ska-forecasting/launch/mg_{0}.sub'.format(name)
          #open(mg_sub_filename,'w').write(mg_sub)
          print "qsub ", mg_sub_filename

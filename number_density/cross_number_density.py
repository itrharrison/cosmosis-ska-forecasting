from cosmosis.datablock import names as section_names, option_section
import numpy as np
import pdb
import photometric_smail as photo
import ska_number_density as ska_num


def setup(options):
	filename = options[option_section, "filename"]
	ngal = options[option_section, "ngal"]
	spectro_fraction = options[option_section, "spectro_fraction"]
	spectro_zmax = options[option_section, "spectro_zmax"]
	photo_zmax = options[option_section, "photo_zmax"]
	photo_err = options[option_section, "photo_err"]
	no_photo_err = options[option_section, "no_photo_err"]
	nbin = options[option_section, "nbin"]
	z, nz = np.loadtxt(filename).T
	zmax=z.max()
	z_interp = np.arange(0.0, zmax, 0.01)
	nz_interp = np.interp(z_interp, z, nz)

	filename2 = options[option_section, "filename2"]
	ngal2 = options[option_section, "ngal2"]
	spectro_fraction2 = options[option_section, "spectro_fraction2"]
	spectro_zmax2 = options[option_section, "spectro_zmax2"]
	photo_zmax2 = options[option_section, "photo_zmax2"]
	photo_err2 = options[option_section, "photo_err2"]
	no_photo_err2 = options[option_section, "no_photo_err2"]
	nbin = options[option_section, "nbin"]
	z2, nz2 = np.loadtxt(filename2).T
	zmax2=z2.max()
	z_interp2 = np.arange(0.0, zmax2, 0.01)
	nz_interp2 = np.interp(z_interp2, z2, nz2)
	return (z_interp, nz_interp, spectro_fraction, 
		spectro_zmax, ngal, nbin,
		photo_zmax, photo_err, no_photo_err,
		z_interp2, nz_interp2, spectro_fraction2, 
		spectro_zmax2, ngal2, nbin,
		photo_zmax2, photo_err2, no_photo_err2,
		)

def execute(block, config):
	#pdb.set_trace()
	(z, nz_true, spectro_fraction, spectro_zmax, ngal, nbin,
	 photo_zmax, photo_err, no_photo_err) = config[:9]
	ska_num.execute(block, config[:9])

	(z, nz_true, spectro_fraction, spectro_zmax, ngal, nbin,
	 photo_zmax, photo_err, no_photo_err) = config[9:]
	ska_num.execute(block, config[9:])
	return 0

import os
import numpy as np
import pdb

start_dir = 'cosmosis-ska-forecasting/start/'

experiments = ['euclid',
               'ska2-large',
               #'euclidxska2'
               ]

nz_experiments = {'euclid' : 'euclid',
    'ska2-large':'ska2_large',
    'euclidxska2':'eucliduxska2'
}

spaces = ['w0wa',
          #'matter',
          'mg']

parameters = {'w0wa' : np.array(['w0', 'wa']),
              'matter' : np.array(['sigma_8', 'omega_m']),
              'mg' : np.array(['q_0', 'd_0'])
              }
values = {'w0' : [-1.0, -1.05, -0.95, -1.025, -0.975],
          'wa' : [0.0, 0.05, -0.05, 0.025, -0.025],
          'sigma_8' : [],
          'omega_m' : [],
          'q_0' : [1.0, 0.95, 1.05, 1.025, 0.975],
          'd_0' : [1.0, 0.95, 1.05, 1.025, 0.975]
          }

for expt in experiments:
  for space in spaces:
    pars = parameters[space]
    for fid in pars:
      val1 = values[fid][0]
      nfid = pars[~(pars==fid)][0]
      variable = values[nfid]
      for val2 in variable:
        name = 'fisher-{0}-{5}-{1}{2}-{3}{4}'.format(expt,
                                                 fid, val1,
                                                 nfid, val2, space)
        nz_expt = nz_experiments[expt]
        ini_filename = 'cosmosis-ska-forecasting/fisher-configuration/{0}.ini'.format(name)
        values_filename = 'cosmosis-ska-forecasting/fisher-analysis/{0}_values.ini'.format(name)

        ini_template = open('cosmosis-ska-forecasting/fisher-configuration/{0}.ini'.format(expt)).read()
        values_template = open('cosmosis-ska-forecasting/fisher-analysis/{0}_values.template'.format(space)).read()

        ini_file = ini_template.format(values_ini=values_filename, name=name)
        if fid==pars[0]:
          values_file = values_template.format(value1=val1, value2=val2)
        else:
          values_file = values_template.format(value1=val2, value2=val1)

        open(ini_filename, 'w').write(ini_file)
        open(values_filename, 'w').write(values_file)

        cmd = 'mpirun -n 1 cosmosis --mpi {0}'.format(ini_filename)
        print(cmd)
        #os.system(cmd)

